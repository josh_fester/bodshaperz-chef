include_recipe 'deploy'
include_recipe "nginx::service"

#
# Deploy the application
#
node[:deploy].each do |application, deploy|
  if deploy[:application_type] != 'static'
    Chef::Log.debug("Skipping deploy::web application #{application} as it is not an static HTML app")
    next
  end

  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path deploy[:deploy_to]
  end

  opsworks_deploy do
    app application
    deploy_data deploy
  end

  nginx_web_app application do
    application deploy
  end
end

#
# Disable the default site
#
execute "nxdissite default" do
  command "/usr/sbin/nxdissite default"
  notifies :reload, "service[nginx]"
  only_if do File.symlink?("#{node[:nginx][:dir]}/sites-enabled/default") end
end
