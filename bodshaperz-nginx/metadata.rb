name             'bodshaperz-nginx'
maintainer       'Josh Fester'
maintainer_email 'josh@bodshaperz.com'
license          'All rights reserved'
description      'Configures nginx site'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'deploy'
depends 'nginx'
